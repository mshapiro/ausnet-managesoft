# == Class: managesoft
#
# This is a puppet module to control Managesoft. (Flexera)
#
#
# === Parameters
# [*managesoft_answer*]
#   This is a temporary response file that will be controlled by Puppet
#   This file will be used to copy to $managesoft_ans_dir when it is require
#   to install managesoft (Flexera).
#   Default: /root/.managesoft/mgsft_rollout_response
#
# [*managesoft_dir*]
#   This is a temporary location for managesoft's answer file that will be controlled by Puppet
#   Default: /root/.managesoft/
#
# [*managesoft_ans_dir*]
#   This is the location where managesoft's installation will look for the answer file.
#   Default: /var/tmp
#
#
# === Authors
#
# AusNet Services Ltd. <root@lpsata10.sysdomain.local>
#
# === Copyright
#
# Copyright 2015 AusNet Services Ltd., unless otherwise noted.
#
class managesoft(
  $beacon_ip)
  {
    $managesoft_answer = '/root/.managesoft/mgsft_rollout_response'
    $managesoft_client_config = '/root/.managesoft/client_config.ini'
    $managesoft_dir = '/root/.managesoft'
    $managesoft_ans_dir = '/var/tmp/'
    
    File["$managesoft_answer"] -> Exec['managesoft_copy_answer']
    File["$managesoft_client_config"] -> Exec['managesoft_copy_client_config']
    Exec['managesoft_copy_answer'] -> Package['managesoft'] 
    Exec['managesoft_copy_client_config'] -> Package['managesoft'] 
    Package['managesoft'] ~> Exec['managesoft_load_client_config']
    Exec['managesoft_load_client_config'] ~> Exec['managesoft_start_ndtrack']

    # resource defaults
    File {
      ensure    =>  'file',
      owner     =>  'root',
      group     =>  'root',
      mode      =>  '0755',
      selrange  =>  's0',
      selrole   =>  'object_r',
      seltype   =>  'admin_home_t',
      seluser   =>  'unconfined_u',
    }
    Exec {
      path      =>  '/usr/bin:/bin:/usr/sbin:/sbin',
    }
    file { $managesoft_dir:
      ensure	=> 'directory',
      mode	=> '0750',
    }
    file { $managesoft_answer:
      ensure	=> 'file',
      content   => template('managesoft/mgsft_rollout_response.erb'),
    }
    file { $managesoft_client_config:
      ensure	=> 'file',
      source	=> 'puppet:///modules/managesoft/client_config.ini',
    }
    exec { 'managesoft_copy_answer':
      command	=> "cp -f $managesoft_answer $managesoft_ans_dir",
      unless    => "diff -q $managesoft_answer $managesoft_ans_dir/mgsft_rollout_response",
    }
    exec { 'managesoft_copy_client_config':
      command	=> "cp -f $managesoft_client_config $managesoft_ans_dir",
      unless    => "diff -q $managesoft_client_config $managesoft_ans_dir/client_config.ini",
    }
    exec { 'managesoft_load_client_config':
      command	=> "/opt/managesoft/bin/mgsconfig -i $managesoft_ans_dir/client_config.ini",
      refreshonly => true,
    }
    exec { 'managesoft_start_ndtrack':
      command	=> "/opt/managesoft/bin/ndtrack -t machine",
      refreshonly => true,
    }
    package { 'managesoft':
      ensure	=> 'present',
      provider	=> 'yum',
    }
}

